public class PatternCheck {
	public static void main(String[] args) {
		System.out.println(repeatedSubstringPattern("abab"));
	}
	
    public static boolean repeatedSubstringPattern(String s) {
        int size = s.length();
        if(size == 1)
            return true;
        for(int i=0; i<= size/2 ;i++){
            if(size%(i+1) == 0 && s.charAt(i)==s.charAt(size-1)){
                 if(checkIfSubStringPattern(s,i+1))
                     return true;
            }
        }
        return false;
    }
    
    public static boolean checkIfSubStringPattern(String s, int i){
        String subStr = s.substring(0,i);
        for(int j=i; j<s.length();j=j+i){
            if(!subStr.equals(s.substring(j,j+i)))
                return false;
        }
        return true;
    }

}

